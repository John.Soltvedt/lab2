package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    int nItemsInFridge = 0;
    int totalSize = 20;
    ArrayList<FridgeItem> ItemsInFridge;
    ArrayList<FridgeItem> ExpiredFood;

    public Fridge(){
        ItemsInFridge = new ArrayList<>();
        ExpiredFood = new ArrayList<>(); 
    }

    public int totalSize(){
        return totalSize;
    }

    public int nItemsInFridge(){
        return nItemsInFridge;
    }


    public boolean placeIn(FridgeItem item){
        if(nItemsInFridge<totalSize){
            ItemsInFridge.add(item);
            nItemsInFridge++;
            if(item.hasExpired()==true){
                ExpiredFood.add(item);
                }
            return true;
        }
        return false;

    }

    public void takeOut(FridgeItem item){
        ArrayList<FridgeItem> TempFridge;
        TempFridge = new ArrayList<>();
        TempFridge.addAll(ItemsInFridge);
        if(nItemsInFridge > 0){
            if(TempFridge.contains(item)){
                nItemsInFridge--;
                ItemsInFridge.remove(item);
            }
        }
        else{
             throw new NoSuchElementException();
        }
    }

    public void emptyFridge(){
        nItemsInFridge = 0;
        ItemsInFridge = new ArrayList<>();
    }

    public List<FridgeItem> removeExpiredFood(){
        int amount_of_expired_food = ExpiredFood.size();
        ArrayList<FridgeItem> removedExpiredFood;
        removedExpiredFood = new ArrayList<>();
        removedExpiredFood.addAll(ExpiredFood);
        ExpiredFood = new ArrayList<>();
        nItemsInFridge -= amount_of_expired_food;
        return removedExpiredFood;
    }
    
    

}


